package test;

import static org.junit.jupiter.api.Assertions.*;

class demoTest {

    @org.junit.jupiter.api.Test
    void getMax() {
        assertEquals(1,demo.getMax(0,1));
        assertEquals(1,demo.getMax(1,0));
        assertEquals(100,demo.getMax(100,99));
    }
}
